import React, { useState } from 'react';
import axios from 'axios';

const LoginForm = () => {
  const [username, setUsername] = useState('');
  const [password, setPassword] = useState('');
  const [isLoggedIn, setIsLoggedIn] = useState(false);
  const [user, setUser] = useState(null);
  const [error, setError] = useState('');

  const handleSubmit = async (e) => {
    e.preventDefault();

    try {
      const response = await axios.post('http://localhost:3005/api/users/login', {
        username: username,
        password: password
      });

      // Assuming the response.data structure is { firstname: 'John', lastname: 'Doe' }
      console.log(response.data);

      // Assuming the response contains firstname and lastname
      if (response.data && response.data.firstname && response.data.lastname) {
        // Set the user state and update isLoggedIn flag
        setUser(response.data);
        setIsLoggedIn(true);
        setError('');
      } else {
        // Handle cases where firstname or lastname is missing in the response
        setError('Firstname or lastname missing in the response');
      }

      // Reset the form fields
      setUsername('');
      setPassword('');
    } catch (error) {
      // Handle errors that occurred during the request
      console.error(error);
      setError('Login failed. Please try again.');
    }
  };

  return (
    <div>
      {isLoggedIn && user && user.firstname && user.lastname ? (
        <div>
          <p>Welcome, {user.firstname} {user.lastname}!</p>
          {/* Display additional user information if needed */}
        </div>
      ) : (
        <form onSubmit={handleSubmit}>
          {error && <p>{error}</p>}
          <label htmlFor="username">Username:</label>
          <input
            type="text"
            id="username"
            value={username}
            onChange={(e) => setUsername(e.target.value)}
            required
          />

          <label htmlFor="password">Password:</label>
          <input
            type="password"
            id="password"
            value={password}
            onChange={(e) => setPassword(e.target.value)}
            required
          />

          <button type="submit">Login</button>
        </form>
      )}
    </div>
  );
};

export default LoginForm;
